module.exports = {
  publicPath: undefined,
  runtimeCompiler: true,
  outputDir: undefined,
  assetsDir: undefined,
  productionSourceMap: undefined,
  parallel: undefined,
  css: undefined
}